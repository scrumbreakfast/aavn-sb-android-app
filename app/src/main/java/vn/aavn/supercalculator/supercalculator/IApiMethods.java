package vn.aavn.supercalculator.supercalculator;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
/**
 * Created by dangducnam on 9/11/15.
 */
public interface IApiMethods {

    @GET("/calculator/add")
    void add(
            @Query("num1") float number1,
            @Query("num2") float number2,
            Callback<CalculateResult> resultCallback
    );
    @GET("/calculator/divide")
    void divide(
            @Query("num1") float number1,
            @Query("num2") float number2,
            @Query("scale") int scale,
            Callback<CalculateResult> resultCallback
    );
    @GET("/calculator/multiply")
    void multiply(
            @Query("num1") float number1,
            @Query("num2") float number2,
            Callback<CalculateResult> resultCallback
    );
    @GET("/calculator/subtract")
    void subtract(
            @Query("num1") float number1,
            @Query("num2") float number2,
            Callback<CalculateResult> resultCallback
    );
}
