package vn.aavn.supercalculator.supercalculator;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    RestAdapter restAdapter;

    TextView txtLabel;
    TextView txt_operator;

    float operand1 = 0.0f;
    float operand2 = 0.0f;

    StringBuilder buffer1 = null;
    StringBuilder buffer2 = null;
    boolean dotAdded = false;

    Operation operator;



    Button btnC;
    Button btnPosNeg;
    Button btnPercentage;
    Button btnDivide;
    Button btnMultiply;
    Button btnAdd;
    Button btnSubtract;
    Button btnEqual;
    Button btnDot;
    Button btnNumber1;
    Button btnNumber2;
    Button btnNumber3;
    Button btnNumber4;
    Button btnNumber5;
    Button btnNumber6;
    Button btnNumber7;
    Button btnNumber8;
    Button btnNumber9;
    Button btnNumber0;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = new ProgressDialog(this);
        progress.setMessage("calculating...");

        txtLabel = (TextView)findViewById(R.id.txtLabel);
        txt_operator  = (TextView)findViewById(R.id.txt_operator);
        btnC = (Button)findViewById(R.id.btnC);
        btnC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clearAll();
            }
        });
        btnPosNeg = (Button)findViewById(R.id.btnPosNeg);
        btnPercentage = (Button)findViewById(R.id.btnPercentage);

        btnDivide = (Button)findViewById(R.id.btnDivide);
        btnDivide.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (buffer1==null) {
                    return;
                } else if (buffer2 == null) {
                    buffer2 = new StringBuilder();
                    dotAdded = false;
                    operator = Operation.DIVIDE;
                    txt_operator.setText(getString(R.string.divide));
                }
            }
        });
        btnMultiply = (Button)findViewById(R.id.btnMultiply);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (buffer1==null) {
                    return;
                } else if (buffer2 == null) {
                    buffer2 = new StringBuilder();
                    dotAdded = false;
                    operator = Operation.ADD;
                    txt_operator.setText(getString(R.string.add));
                }
            }
        });
        btnEqual = (Button)findViewById(R.id.btnEqual);
        btnEqual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (buffer1!=null && buffer1.length()>0 && buffer2!=null && buffer2.length()>0) {
                    progress.show();
                    callService(operator, buffer1.toString(), buffer2.toString());
                }
            }
        });
        btnSubtract = (Button)findViewById(R.id.btnSubtract);
        btnDot = (Button)findViewById(R.id.btnDot);
        btnDot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('.');
            }
        });
        btnNumber1 = (Button)findViewById(R.id.btnNumber1);
        btnNumber1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('1');
            }
        });
        btnNumber2 = (Button)findViewById(R.id.btnNumber2);
        btnNumber2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('2');
            }
        });

        btnNumber3 = (Button)findViewById(R.id.btnNumber3);
        btnNumber3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('3');
            }
        });

        btnNumber4 = (Button)findViewById(R.id.btnNumber4);
        btnNumber4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('4');
            }
        });

        btnNumber5 = (Button)findViewById(R.id.btnNumber5);
        btnNumber5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('5');
            }
        });

        btnNumber6 = (Button)findViewById(R.id.btnNumber6);
        btnNumber6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('6');
            }
        });

        btnNumber7 = (Button)findViewById(R.id.btnNumber7);
        btnNumber7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('7');
            }
        });

        btnNumber8 = (Button)findViewById(R.id.btnNumber8);
        btnNumber8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('8');
            }
        });

        btnNumber9 = (Button)findViewById(R.id.btnNumber9);
        btnNumber9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('9');
            }
        });

        btnNumber0 = (Button)findViewById(R.id.btnNumber0);
        btnNumber0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addBuffer('0');
            }
        });

        checkForUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateManager.unregister();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    public void callService(Operation calling_operator, String bf1, String bf2) {
        operand1 = Float.parseFloat(bf1);
        operand2 = Float.parseFloat(bf2);
        Toast.makeText(getBaseContext(), getString(R.string.API_URL), Toast.LENGTH_LONG).show();
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(getString(R.string.API_URL))
                .build();
        IApiMethods methods = restAdapter.create(IApiMethods.class);

        Callback callback = new Callback<CalculateResult>() {
            @Override
            public void success(CalculateResult result, Response response) {
                if (result.error!=null) {
                    txtLabel.setText(result.error);
                } else {
                    txtLabel.setText(Float.toString(result.result));
                }

                resetValues();
                progress.dismiss();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(MainActivity.this, retrofitError.getMessage(), Toast.LENGTH_LONG).show();;
                progress.dismiss();
            }

        };

        if (calling_operator==Operation.ADD) {
            methods.add(operand1, operand2, callback);
        } else if (calling_operator==Operation.DIVIDE) {
            methods.divide(operand1, operand2, 3, callback);
        } else if (calling_operator==Operation.MULTIPLY) {
            methods.multiply(operand1, operand2, callback);
        } else if (calling_operator==Operation.SUBTRACT) {
            methods.subtract(operand1, operand2, callback);
        }

    }
    private void addBuffer(char number) {
        if (dotAdded && number=='.') {
            //do nothing, return
            return;
        }
        if ((buffer1!=null && buffer2==null && buffer1.length()>10) || (buffer2!=null && buffer2.length()>10)) {
            return;
        }
        if (buffer1==null) {
            buffer1 = new StringBuilder();
            if (number!='.') {
                buffer1.append(number);
            } else {
                buffer1.append('0');
                buffer1.append(number);
                dotAdded = true;
            }
            txtLabel.setText(buffer1.toString());
        } else if (buffer2==null) {
            if (number=='.') {
                dotAdded = true;
            }
            buffer1.append(number);
            txtLabel.setText(buffer1.toString());
        } else {
            if (number!='.') {
                buffer2.append(number);
            } else {
                if (!dotAdded) {
                    if (buffer2.length()==0)
                        buffer2.append('0');
                    dotAdded = true;
                    buffer2.append(number);
                }
            }
            txtLabel.setText(buffer2.toString());
        }
    }
    private void clearAll() {
        resetValues();
        txtLabel.setText(null);
    }
    private void resetValues() {
        operand1 = 0.0f;
        operand2 = 0.0f;
        buffer1 = null;
        buffer2 = null;
        dotAdded = false;
        txt_operator.setText(null);
    }
    private void checkForCrashes() {
        CrashManager.register(this, getString(R.string.HockeyAppID));
    }

    private void checkForUpdates() {
        // Remove this for store / production builds!
        UpdateManager.register(this, getString(R.string.HockeyAppID));
    }

}
