package vn.aavn.supercalculator.supercalculator;

/**
 * Created by dangducnam on 9/11/15.
 */
public enum Operation {
    ADD, MULTIPLY, DIVIDE, SUBTRACT
}
