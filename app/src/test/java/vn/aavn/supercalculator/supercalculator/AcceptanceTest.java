package vn.aavn.supercalculator.supercalculator;

import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.Button;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


/**
 * Created by hoangnguyen on 9/17/15.
 */

public class AcceptanceTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Button addValues;
    private Button multiplyValues;
    private View mainLayout;

    public AcceptanceTest() {
        super("vn.aavn.supercalculator.supercalculator", MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        setActivityInitialTouchMode(false);

        MainActivity mainActivity = getActivity();

    }

    private static final String ADD_RESULT = "12.0";


    public void testAddValues() {

        onView(withId(R.id.btnNumber5)).perform(click());
        onView(withId(R.id.btnAdd)).perform(click());
        onView(withId(R.id.btnNumber7)).perform(click());
        onView(withId(R.id.btnEqual)).perform(click());

        pauseTestFor(2000);

        onView(withId(R.id.txtLabel))
                .check(matches(withText(ADD_RESULT)));
    }

    private void pauseTestFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
